#include "common.h"

msg_common_ans_get_t decode_common_ans_get(CANdata msg)
{
    msg_common_ans_get_t msg_struct;
    msg_struct.data = decode_common_ans_get_data(msg);
    msg_struct.dst = decode_common_ans_get_dst(msg);
    msg_struct.id = decode_common_ans_get_id(msg);

    return msg_struct;
}

msg_common_ans_set_t decode_common_ans_set(CANdata msg)
{
    msg_common_ans_set_t msg_struct;
    msg_struct.data = decode_common_ans_set_data(msg);
    msg_struct.dst = decode_common_ans_set_dst(msg);
    msg_struct.id = decode_common_ans_set_id(msg);

    return msg_struct;
}

msg_common_log_t decode_common_log(CANdata msg)
{
    msg_common_log_t msg_struct;
    msg_struct.arg1 = decode_common_log_arg1(msg);
    msg_struct.arg2 = decode_common_log_arg2(msg);
    msg_struct.arg3 = decode_common_log_arg3(msg);
    msg_struct.err_code = decode_common_log_err_code(msg);
    msg_struct.level = decode_common_log_level(msg);
    msg_struct.n_args = decode_common_log_n_args(msg);

    return msg_struct;
}

msg_common_req_get_t decode_common_req_get(CANdata msg)
{
    msg_common_req_get_t msg_struct;
    msg_struct.dst = decode_common_req_get_dst(msg);
    msg_struct.id = decode_common_req_get_id(msg);

    return msg_struct;
}

msg_common_req_set_t decode_common_req_set(CANdata msg)
{
    msg_common_req_set_t msg_struct;
    msg_struct.data = decode_common_req_set_data(msg);
    msg_struct.dst = decode_common_req_set_dst(msg);
    msg_struct.id = decode_common_req_set_id(msg);

    return msg_struct;
}

msg_common_return_cmd_t decode_common_return_cmd(CANdata msg)
{
    msg_common_return_cmd_t msg_struct;
    msg_struct.id = decode_common_return_cmd_id(msg);
    msg_struct.ret1 = decode_common_return_cmd_ret1(msg);
    msg_struct.ret2 = decode_common_return_cmd_ret2(msg);
    msg_struct.ret3 = decode_common_return_cmd_ret3(msg);

    return msg_struct;
}

msg_common_send_cmd_t decode_common_send_cmd(CANdata msg)
{
    msg_common_send_cmd_t msg_struct;
    msg_struct.arg1 = decode_common_send_cmd_arg1(msg);
    msg_struct.arg2 = decode_common_send_cmd_arg2(msg);
    msg_struct.arg3 = decode_common_send_cmd_arg3(msg);
    msg_struct.dst = decode_common_send_cmd_dst(msg);
    msg_struct.id = decode_common_send_cmd_id(msg);

    return msg_struct;
}

void decode_common(CANdata msg, dev_common_t* dev)
{
    switch (msg.msg_id) {
    case 4:
        dev->ans_get = decode_common_ans_get(msg);
        break;
    case 6:
        dev->ans_set = decode_common_ans_set(msg);
        break;
    case 0:
        dev->log = decode_common_log(msg);
        break;
    case 3:
        dev->req_get = decode_common_req_get(msg);
        break;
    case 5:
        dev->req_set = decode_common_req_set(msg);
        break;
    case 2:
        dev->return_cmd = decode_common_return_cmd(msg);
        break;
    case 1:
        dev->send_cmd = decode_common_send_cmd(msg);
        break;
    }
}

CANdata encode_common_ans_get(msg_common_ans_get_t msg, uint16_t dev_id)
{
    uint64_t word = 0;
    CANdata message;
    message.msg_id = 4;
    message.dev_id = dev_id;
    message.dlc = 8;
    uint64_t* ptr = (uint64_t*)&message.data;

    word |= encode_common_ans_get_data(msg.data);

    word |= encode_common_ans_get_dst(msg.dst);

    word |= encode_common_ans_get_id(msg.id);

    *ptr = word;
    return message;
}

CANdata encode_common_ans_set(msg_common_ans_set_t msg, uint16_t dev_id)
{
    uint64_t word = 0;
    CANdata message;
    message.msg_id = 6;
    message.dev_id = dev_id;
    message.dlc = 8;
    uint64_t* ptr = (uint64_t*)&message.data;

    word |= encode_common_ans_set_data(msg.data);

    word |= encode_common_ans_set_dst(msg.dst);

    word |= encode_common_ans_set_id(msg.id);

    *ptr = word;
    return message;
}

CANdata encode_common_log(msg_common_log_t msg, uint16_t dev_id)
{
    uint64_t word = 0;
    CANdata message;
    message.msg_id = 0;
    message.dev_id = dev_id;
    message.dlc = 8;
    uint64_t* ptr = (uint64_t*)&message.data;

    word |= encode_common_log_arg1(msg.arg1);

    word |= encode_common_log_arg2(msg.arg2);

    word |= encode_common_log_arg3(msg.arg3);

    word |= encode_common_log_err_code(msg.err_code);

    word |= encode_common_log_level(msg.level);

    word |= encode_common_log_n_args(msg.n_args);

    *ptr = word;
    return message;
}

CANdata encode_common_req_get(msg_common_req_get_t msg, uint16_t dev_id)
{
    uint64_t word = 0;
    CANdata message;
    message.msg_id = 3;
    message.dev_id = dev_id;
    message.dlc = 8;
    uint64_t* ptr = (uint64_t*)&message.data;

    word |= encode_common_req_get_dst(msg.dst);

    word |= encode_common_req_get_id(msg.id);

    *ptr = word;
    return message;
}

CANdata encode_common_req_set(msg_common_req_set_t msg, uint16_t dev_id)
{
    uint64_t word = 0;
    CANdata message;
    message.msg_id = 5;
    message.dev_id = dev_id;
    message.dlc = 8;
    uint64_t* ptr = (uint64_t*)&message.data;

    word |= encode_common_req_set_data(msg.data);

    word |= encode_common_req_set_dst(msg.dst);

    word |= encode_common_req_set_id(msg.id);

    *ptr = word;
    return message;
}

CANdata encode_common_return_cmd(msg_common_return_cmd_t msg, uint16_t dev_id)
{
    uint64_t word = 0;
    CANdata message;
    message.msg_id = 2;
    message.dev_id = dev_id;
    message.dlc = 8;
    uint64_t* ptr = (uint64_t*)&message.data;

    word |= encode_common_return_cmd_id(msg.id);

    word |= encode_common_return_cmd_ret1(msg.ret1);

    word |= encode_common_return_cmd_ret2(msg.ret2);

    word |= encode_common_return_cmd_ret3(msg.ret3);

    *ptr = word;
    return message;
}

CANdata encode_common_send_cmd(msg_common_send_cmd_t msg, uint16_t dev_id)
{
    uint64_t word = 0;
    CANdata message;
    message.msg_id = 1;
    message.dev_id = dev_id;
    message.dlc = 8;
    uint64_t* ptr = (uint64_t*)&message.data;

    word |= encode_common_send_cmd_arg1(msg.arg1);

    word |= encode_common_send_cmd_arg2(msg.arg2);

    word |= encode_common_send_cmd_arg3(msg.arg3);

    word |= encode_common_send_cmd_dst(msg.dst);

    word |= encode_common_send_cmd_id(msg.id);

    *ptr = word;
    return message;
}
