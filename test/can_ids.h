#ifndef __CAN_IDS_H__
#define __CAN_IDS_H__

#include "common.h"

/* LOG IDs */
#define LOG_WRONG_LOG_ID 0
#define LOG_WRONG_CFG_ID 1
#define LOG_HIGH_TEMP 2
#define LOG_WRONG_CMD_ID 3
#define LOG_TRAP_HANDLER_TYPE 4
#define LOG_RESET_MESSAGE 5
#define LOG_NTC_NUMBER 6
#define LOG_FLOW_STATE 7
#define LOG_MOTOR_STATE 8
#define LOG_RTD_ON 9
#define LOG_RTD_OFF 10
#define LOG_TS_STATE 11
#define LOG_IIB_WTD_EXT 12

#endif
