#ifndef __COMMON__H_
#define __COMMON__H_

#include <stdint.h>

#include "candata.h"
#include "signal_parser.h"


#define MSG_ID_COMMON_LOG 0
#define MSG_ID_COMMON_SEND_CMD 1
#define MSG_ID_COMMON_RETURN_CMD 2
#define MSG_ID_COMMON_REQ_GET 3
#define MSG_ID_COMMON_ANS_GET 4
#define MSG_ID_COMMON_REQ_SET 5
#define MSG_ID_COMMON_ANS_SET 6

typedef struct _msg_common_ans_get_t {
    uint32_t data;
    uint8_t dst;
    uint8_t id;
} msg_common_ans_get_t;
typedef struct _msg_common_ans_set_t {
    uint32_t data;
    uint8_t dst;
    uint8_t id;
} msg_common_ans_set_t;
typedef struct _msg_common_log_t {
    uint16_t arg1;
    uint16_t arg2;
    uint16_t arg3;
    uint8_t err_code;
    uint8_t level;
    uint8_t n_args;
} msg_common_log_t;
typedef struct _msg_common_req_get_t {
    uint8_t dst;
    uint8_t id;
} msg_common_req_get_t;
typedef struct _msg_common_req_set_t {
    uint32_t data;
    uint8_t dst;
    uint8_t id;
} msg_common_req_set_t;
typedef struct _msg_common_return_cmd_t {
    uint8_t id;
    uint16_t ret1;
    uint16_t ret2;
    uint16_t ret3;
} msg_common_return_cmd_t;
typedef struct _msg_common_send_cmd_t {
    uint16_t arg1;
    uint16_t arg2;
    uint16_t arg3;
    uint8_t dst;
    uint8_t id;
} msg_common_send_cmd_t;

typedef struct _dev_common_t {

    msg_common_ans_get_t ans_get;
    msg_common_ans_set_t ans_set;
    msg_common_log_t log;
    msg_common_req_get_t req_get;
    msg_common_req_set_t req_set;
    msg_common_return_cmd_t return_cmd;
    msg_common_send_cmd_t send_cmd;
} dev_common_t;


#define decode_common_ans_get_data(msg) decode_signal_unsigned_as_uint32_t((msg), 16, 32, 1.0, 0.0);

#define decode_common_ans_get_dst(msg) decode_signal_unsigned_as_uint8_t((msg), 8, 8, 1.0, 0.0);

#define decode_common_ans_get_id(msg) decode_signal_unsigned_as_uint8_t((msg), 0, 5, 1.0, 0.0);

#define decode_common_ans_set_data(msg) decode_signal_unsigned_as_uint32_t((msg), 16, 32, 1.0, 0.0);

#define decode_common_ans_set_dst(msg) decode_signal_unsigned_as_uint8_t((msg), 8, 8, 1.0, 0.0);

#define decode_common_ans_set_id(msg) decode_signal_unsigned_as_uint8_t((msg), 0, 5, 1.0, 0.0);

#define decode_common_log_arg1(msg) decode_signal_unsigned_as_uint16_t((msg), 16, 16, 1.0, 0.0);

#define decode_common_log_arg2(msg) decode_signal_unsigned_as_uint16_t((msg), 32, 16, 1.0, 0.0);

#define decode_common_log_arg3(msg) decode_signal_unsigned_as_uint16_t((msg), 48, 16, 1.0, 0.0);

#define decode_common_log_err_code(msg) decode_signal_unsigned_as_uint8_t((msg), 8, 8, 1.0, 0.0);

#define decode_common_log_level(msg) decode_signal_unsigned_as_uint8_t((msg), 0, 3, 1.0, 0.0);

#define decode_common_log_n_args(msg) decode_signal_unsigned_as_uint8_t((msg), 3, 2, 1.0, 0.0);

#define decode_common_req_get_dst(msg) decode_signal_unsigned_as_uint8_t((msg), 0, 5, 1.0, 0.0);

#define decode_common_req_get_id(msg) decode_signal_unsigned_as_uint8_t((msg), 8, 8, 1.0, 0.0);

#define decode_common_req_set_data(msg) decode_signal_unsigned_as_uint32_t((msg), 16, 32, 1.0, 0.0);

#define decode_common_req_set_dst(msg) decode_signal_unsigned_as_uint8_t((msg), 0, 5, 1.0, 0.0);

#define decode_common_req_set_id(msg) decode_signal_unsigned_as_uint8_t((msg), 8, 8, 1.0, 0.0);

#define decode_common_return_cmd_id(msg) decode_signal_unsigned_as_uint8_t((msg), 8, 8, 1.0, 0.0);

#define decode_common_return_cmd_ret1(msg) decode_signal_unsigned_as_uint16_t((msg), 16, 16, 1.0, 0.0);

#define decode_common_return_cmd_ret2(msg) decode_signal_unsigned_as_uint16_t((msg), 32, 16, 1.0, 0.0);

#define decode_common_return_cmd_ret3(msg) decode_signal_unsigned_as_uint16_t((msg), 48, 16, 1.0, 0.0);

#define decode_common_send_cmd_arg1(msg) decode_signal_unsigned_as_uint16_t((msg), 16, 16, 1.0, 0.0);

#define decode_common_send_cmd_arg2(msg) decode_signal_unsigned_as_uint16_t((msg), 32, 16, 1.0, 0.0);

#define decode_common_send_cmd_arg3(msg) decode_signal_unsigned_as_uint16_t((msg), 48, 16, 1.0, 0.0);

#define decode_common_send_cmd_dst(msg) decode_signal_unsigned_as_uint8_t((msg), 0, 5, 1.0, 0.0);

#define decode_common_send_cmd_id(msg) decode_signal_unsigned_as_uint8_t((msg), 8, 8, 1.0, 0.0);

msg_common_ans_get_t decode_common_ans_get(CANdata msg);
msg_common_ans_set_t decode_common_ans_set(CANdata msg);
msg_common_log_t decode_common_log(CANdata msg);
msg_common_req_get_t decode_common_req_get(CANdata msg);
msg_common_req_set_t decode_common_req_set(CANdata msg);
msg_common_return_cmd_t decode_common_return_cmd(CANdata msg);
msg_common_send_cmd_t decode_common_send_cmd(CANdata msg);

void decode_common(CANdata msg, dev_common_t* dev);

CANdata encode_common_ans_get(msg_common_ans_get_t, uint16_t dev_id);
CANdata encode_common_ans_set(msg_common_ans_set_t, uint16_t dev_id);
CANdata encode_common_log(msg_common_log_t, uint16_t dev_id);
CANdata encode_common_req_get(msg_common_req_get_t, uint16_t dev_id);
CANdata encode_common_req_set(msg_common_req_set_t, uint16_t dev_id);
CANdata encode_common_return_cmd(msg_common_return_cmd_t, uint16_t dev_id);
CANdata encode_common_send_cmd(msg_common_send_cmd_t, uint16_t dev_id);

#define encode_common_ans_get_data(signal) encode_signal_unsigned_from_uint32_t((signal), 16, 32, 1.0, 0.0);

#define encode_common_ans_get_dst(signal) encode_signal_unsigned_from_uint8_t((signal), 8, 8, 1.0, 0.0);

#define encode_common_ans_get_id(signal) encode_signal_unsigned_from_uint8_t((signal), 0, 5, 1.0, 0.0);

#define encode_common_ans_set_data(signal) encode_signal_unsigned_from_uint32_t((signal), 16, 32, 1.0, 0.0);

#define encode_common_ans_set_dst(signal) encode_signal_unsigned_from_uint8_t((signal), 8, 8, 1.0, 0.0);

#define encode_common_ans_set_id(signal) encode_signal_unsigned_from_uint8_t((signal), 0, 5, 1.0, 0.0);

#define encode_common_log_arg1(signal) encode_signal_unsigned_from_uint16_t((signal), 16, 16, 1.0, 0.0);

#define encode_common_log_arg2(signal) encode_signal_unsigned_from_uint16_t((signal), 32, 16, 1.0, 0.0);

#define encode_common_log_arg3(signal) encode_signal_unsigned_from_uint16_t((signal), 48, 16, 1.0, 0.0);

#define encode_common_log_err_code(signal) encode_signal_unsigned_from_uint8_t((signal), 8, 8, 1.0, 0.0);

#define encode_common_log_level(signal) encode_signal_unsigned_from_uint8_t((signal), 0, 3, 1.0, 0.0);

#define encode_common_log_n_args(signal) encode_signal_unsigned_from_uint8_t((signal), 3, 2, 1.0, 0.0);

#define encode_common_req_get_dst(signal) encode_signal_unsigned_from_uint8_t((signal), 0, 5, 1.0, 0.0);

#define encode_common_req_get_id(signal) encode_signal_unsigned_from_uint8_t((signal), 8, 8, 1.0, 0.0);

#define encode_common_req_set_data(signal) encode_signal_unsigned_from_uint32_t((signal), 16, 32, 1.0, 0.0);

#define encode_common_req_set_dst(signal) encode_signal_unsigned_from_uint8_t((signal), 0, 5, 1.0, 0.0);

#define encode_common_req_set_id(signal) encode_signal_unsigned_from_uint8_t((signal), 8, 8, 1.0, 0.0);

#define encode_common_return_cmd_id(signal) encode_signal_unsigned_from_uint8_t((signal), 8, 8, 1.0, 0.0);

#define encode_common_return_cmd_ret1(signal) encode_signal_unsigned_from_uint16_t((signal), 16, 16, 1.0, 0.0);

#define encode_common_return_cmd_ret2(signal) encode_signal_unsigned_from_uint16_t((signal), 32, 16, 1.0, 0.0);

#define encode_common_return_cmd_ret3(signal) encode_signal_unsigned_from_uint16_t((signal), 48, 16, 1.0, 0.0);

#define encode_common_send_cmd_arg1(signal) encode_signal_unsigned_from_uint16_t((signal), 16, 16, 1.0, 0.0);

#define encode_common_send_cmd_arg2(signal) encode_signal_unsigned_from_uint16_t((signal), 32, 16, 1.0, 0.0);

#define encode_common_send_cmd_arg3(signal) encode_signal_unsigned_from_uint16_t((signal), 48, 16, 1.0, 0.0);

#define encode_common_send_cmd_dst(signal) encode_signal_unsigned_from_uint8_t((signal), 0, 5, 1.0, 0.0);

#define encode_common_send_cmd_id(signal) encode_signal_unsigned_from_uint8_t((signal), 8, 8, 1.0, 0.0);


#endif
