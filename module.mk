MODULE_C_SOURCES := as_can.c
MODULE_C_SOURCES += can_cfg.c
MODULE_C_SOURCES += can_cmd.c
MODULE_C_SOURCES += can_ids.c
MODULE_C_SOURCES += can_log.c
MODULE_C_SOURCES += common.c
MODULE_C_SOURCES += dash_can.c
MODULE_C_SOURCES += ebs_can.c
MODULE_C_SOURCES += etas_can.c
MODULE_C_SOURCES += hw_can.c
MODULE_C_SOURCES += iib_can.c
MODULE_C_SOURCES += interface_can.c
MODULE_C_SOURCES += isabel_can.c
MODULE_C_SOURCES += master_can.c
MODULE_C_SOURCES += res_can.c
MODULE_C_SOURCES += signal_parser.c
MODULE_C_SOURCES += sw_can.c
MODULE_C_SOURCES += te_can.c
MODULE_C_SOURCES += telemetry_can.c
MODULE_C_SOURCES += xsens_can.c
MODULE_C_SOURCES += pdu_can.c
MODULE_C_SOURCES += ami_can.c
MODULE_C_SOURCES += sta_can.c
MODULE_C_SOURCES += suspot_can.c
MODULE_C_SOURCES += pitot_tube_can.c
MODULE_C_SOURCES += wc_can.c
MODULE_C_SOURCES += netas_can.c
MODULE_C_SOURCES += gss_can.c
MODULE_C_SOURCES += strain_gauges_can.c


PWD:=lib/can-ids-v2/
C_SOURCES := $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
